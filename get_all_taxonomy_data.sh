#!/bin/bash
#Usage;
#bash get_all_taxonomy_data.sh email@zzz.com all_ids.txt 5 taxonomy/

#export PATH=$PATH:/home/rmazloom/idea_projects/linbase/gtdb/
email="$1"
id_file=$(realpath $2)
total="$(wc -l $id_file | cut -f 1 -d ' ')"
column="$3"
outdir=$(realpath $4)
count=0
mkdir -p $outdir
while IFS="" read -r line || [ -n "$line" ]
do
	assembly=$(echo $line | cut -f 1 -d " ")
	taxa=$(echo $line | cut -f $column -d " ") #biosampleID
	#bioproject=$(echo $line | cut -f 3 -d " ")
	#taxa=$(echo $line | cut -f 4 -d " ")
	if ! [ -s ${outdir}/${assembly}.json ]
	then
		efetch -email $email -db taxonomy -id "$taxa" -mode xml -json > $outdir/"$assembly".json
		sleep 3;
	fi
	count=$(( $count + 1 ));
	echo "$count / $total";
done < $id_file
