#!/usr/bin/env python3
import csv
import os
import sys
import re

import numpy as np
import pandas as pd

import extensions

GTDB_COLUMNS_TO_KEEP = {
    "accession": "accession",
    "ambiguous_bases": "checkm_ambiguous_bases",
    "checkm_completeness": "checkm_completeness",
    "checkm_contamination": "checkm_contamination",
    "n50_contigs": "checkm_n50_contigs",
    "n50_scaffolds": "checkm_n50_scaffolds",
    "contig_count": "checkm_contig_count",
    "genome_size": "checkm_genome_size",
    "scaffold_count": "checkm_scaffold_count",
    "gtdb_taxonomy": "gtdb_taxonomy",
    "gtdb_type_designation_ncbi_taxa": "gtdb_type_designation",
    "gtdb_representative": "gtdb_representative",
    "ncbi_organism_name": "ncbi_organism_name",
    "ncbi_taxonomy": "ncbi_taxonomy_filtered",
    "ncbi_type_material_designation": "ncbi_type_material_designation",
    "ncbi_species_taxid": "ncbi_species_taxid",
}


def main(ncbi_file, gtdb_file, lin_summary_file=None):

    ncbi_df = pd.read_csv(
        ncbi_file,
        header=0,
        sep="\t",
        parse_dates=False,
        dtype=str,
        skip_blank_lines=True,
    )
    print("read ncbi")
    gtdb_df = pd.read_csv(
        gtdb_file,
        header=0,
        sep="\t",
        usecols=list(GTDB_COLUMNS_TO_KEEP.keys()),
        parse_dates=False,
        dtype=str,
        skip_blank_lines=True,
    )
    print("read gtdb")

    gtdb_df.rename(columns={"accession": "accession_old"}, inplace=True)
    gtdb_df["accession"] = gtdb_df["accession_old"].apply(lambda x: x[3:])
    ncbi_df.rename(columns={"Unnamed: 0": "accession"}, inplace=True)

    """
    print("checking missing")
    if missing := len(np.setdiff1d(ncbi_df["accession"], gtdb_df["accession"])) > 0:
        print(f"Some accessions are missing from the NCBI list {missing}")"""

    print("merging")
    merge_df = ncbi_df.merge(
        gtdb_df[list(GTDB_COLUMNS_TO_KEEP.keys()) + ["accession_old"]],
        how="outer",
        on="accession",
        #    validate="1:1",
    )

    merge_df.rename(columns=GTDB_COLUMNS_TO_KEEP, inplace=True)

    if lin_summary_file:
        lin_df = ncbi_df = pd.read_csv(
            lin_summary_file,
            header=0,
            sep=",",
            parse_dates=False,
            skip_blank_lines=True,
        )
        lin_df["assemblyID"] = lin_df["filePath"].apply(
            lambda x: (
                "_".join(re.sub(r"_genomic.fna.gz$", "", x).split("_")[:2])[4:-2]
                if re.search(r"GC[AF]_", x)
                else extensions.remove_extension(x)
            )
        )
        merge_df["general_assembly_acc"] = merge_df["accession"].apply(
            lambda x: x[4:-2]
        )
        merge_df["accession_version"] = merge_df["accession"].apply(
            lambda x: x[x.rfind(".")+1:]
        )

        # removing accession duplicates but keeping the row+data of the latest version
        merge_df_single_version = merge_df.sort_values('accession_version', ascending=False).drop_duplicates('general_assembly_acc').sort_index()

        merge_df2 = lin_df.merge(
            merge_df_single_version,
            how="left",
            right_on="general_assembly_acc",
            left_on="assemblyID",
            #    validate="1:1"
        )
        print("read lins")
        merge_df2[np.invert(pd.notna(merge_df2["LIN"]))].to_csv(
            "attributes_merged_nolin.tsv", sep="\t"
        )

    merge_df2.to_csv("attributes_merged.tsv", sep="\t")
    merge_df2.to_csv("attributes_merged.csv", sep=",", quoting=csv.QUOTE_ALL)

    print(f"Merge Saved to '{os.path.abspath('attributes_merged.tsv')}'")


if __name__ == "__main__":
    # TSV files
    ncbi_file = os.path.realpath(sys.argv[1])
    gtdb_file = os.path.realpath(sys.argv[2])
    # CSV file
    lin_file = os.path.realpath(sys.argv[3]) if len(sys.argv) > 3 else None

    for file in [ncbi_file, gtdb_file]:
        if not os.path.isfile(file):
            print(f"File '{file}' does not exist")
            exit(-1)

    main(ncbi_file, gtdb_file, lin_file)
