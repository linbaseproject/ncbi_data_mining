#!/bin/bash

#export PATH=$PATH:/home/rmazloom/idea_projects/linbase/gtdb/
total=$(wc -l all_ids.txt)
count=1
category="biosample"
while read line
do
	assembly=$(echo $line | cut -f 1 -d " ")
	id=$(echo $line | cut -f 2 -d " ")
	bioproject=$(echo $line | cut -f 3 -d " ")
	if ! [ -s ${category}/${assembly}.json ]
	then
		esearch -email rmazloom@vt.edu -db $category -query $id </dev/null| efetch -format docsum -json > ${category}/${assembly}.json
		sleep 3
		echo "$count / $total"
	fi
	count=$(( $count + 1 ))
done < all_ids.txt
