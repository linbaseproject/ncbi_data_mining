# [NCBI AutoAPI](#ncbi-autoapi)

These scripts can be used to automate the metadata download from NCBI using the public API (entrez-direct)
package from NCBI and export them into tabular formats.

## [For the Impatient](#for-the-impatient)

I would highly recommend running the `get_all_data.sh` multiple times to cover all the entries (especially for the assembly run)
since sometimes the requests to NCBI could fail for minor reasons excluding information.

```bash
bash get_all_data.sh email@example.edu assembly query.txt 1 data/assembly \
python read_data.py ids data/assembly \
bash get_all_data.sh email@example.edu biosample all_ids.dsv 3 data/biosample \
bash get_all_data.sh email@example.edu taxonomy all_ids.dsv 6 data/taxonomy \
python read_data.py all data/assembly data/biosample data/taxonomy
```

## [Table of Contents](#table-of-contents)

[TOC]

## [1 Installation](#1-installation)

To get access to the latest entrez-direct release you need to install Anaconda.
A minimalist approach is to install Miniconda for python3 available
[here](https://docs.conda.io/en/latest/miniconda.html#linux-installers).
Next, you can create and activate a virtual environment.

### [1.1 NON ARM Create Python environment using Conda (Miniconda)](#11-non-arm-create-python-environment-using-conda-miniconda)

You can now create and activate a virtual environment.
Required python version 3.9+.

```bash
conda create -n ncbi_aapi python=3.10
conda activate ncbi_aapi
```

### [1.1 ARM ONLY Create Python environment using Conda](#11-arm-only-create-python-environment-using-conda)

You can now create and activate a virtual environment.

Required python version 3.9+.

```bash
CONDA_SUBDIR=osx-arm64 conda create -n ncbi_aapi python=3.10
conda activate ncbi_aapi
conda config --env --set subdir osx-arm64
```

### [1.2 Conda Module installation](#12-conda-module-installation)

With the conda virtual environment active, you can install the required modules.

```bash
conda install bioconda::entrez-direct -y \
conda install pandas -y \
conda install -c conda-forge perl-xml-parser -y \
cpan install XML::Simple
```

### [1.3 Clone the repository](#13-clone-the-repository)

You can now clone the repository

```shell
git clone https://code.vt.edu/linbaseproject/ncbi_data_mining
```

## [2 Using the NCBI AutoAPI](#2-using-the-ncbi-autoapi)

1. Create a file with each line containing one assembly or accession number that you would like to find (query). Here we have called the file `tests/data/query.txt`.
1. In the commands below please **replace your email** with the example given `email@example.edu` or you might get blocked by NCBI.
1. Create the directory for the downloaded files which we have named as `data` here

**Suggestion:If you have more than 10,000 queries see [Section 3.1](#31-parallelizing-the-ncbi-api-requests) on how to easily parallelize the code with a few easy steps.**

### [2.1 Downloading assembly data](#21-downloading-assembly-data)

The following command will download the assembly data as well as the connected database IDs such as NCBI BioSample and Taxonomy identifiers.
`data/assembly` will be the location where the data files (`queryID.json`) files are saved.
See [Section 3.7](#37-get_all_datash-parameters) for more information on the parameters.

**IMPORTANT:I would highly recommend running the `get_all_data.sh` multiple times to cover all the entries (especially for the assembly run)
since sometimes the requests to NCBI could fail for minor reasons excluding information.**

```bash
bash get_all_data.sh email@example.edu assembly tests/data/query.txt 1 data/assembly
```

### [2.2 Extracting linked IDs using assembly data](#22-extracting-linked-ids-using-assembly-data)

This step will create a file called `all_ids.dsv` that includes `queryID,BioProjectID,BioSampleID,AccessionNo,AssemblyID,TaxID`.

```bash
python read_data.py ids data/assembly
```

### [2.3 Downloading biosample and taxonomy data](#23-downloading-biosample-and-taxonomy-data)

The following command will download BioSample and Taxonomy data respectively using the file from the previous step.

```bash
bash get_all_data.sh email@example.edu biosample all_ids.dsv 3 data/biosample
bash get_all_data.sh email@example.edu taxonomy all_ids.dsv 6 data/taxonomy
```

### [2.4 Tabulating all data](#24-tabulating-all-data)

This steps combined and transforms the data into tabular format.
You do not need to have all three data directories since only the ones available will be used (i.e. you can only use taxonomy if you like).
The **order of the directories matters** since the data from the earlier directories (to the left) will be given priority and not overwritten for each field (if not empty).
See [Section 3.4-3.6](#34-adding-static-fields-to-attributes) if you need more or less columns (fields) than the default.

```bash
python read_data.py all data/assembly data/biosample data/taxonomy
```

### [2.5 Result files](#25-result-files)

There will be two tabular tabular files `attributes.csv` and `attributes.tsv` with the columns (fields) found.
And there will be one file for each directory named `*_counts.tsv` for the possible fields that were available
in the data files with their respective frequency (how many genomes had the field) in the data set in case
you would like to add more fields to the table and statistical purposes.
See [Section 3.4-3.6](#34-adding-static-fields-to-attributes) if you need more or less fields than the default.

### [2.6 Merging NCBI and GTDB (Bacteria and Archea Only)](#26-merging-ncbi-and-gtdb-bacteria-and-archea-only)

If you have Bacteria or Archea data you can also download them from the Genome Taxonomy Database (GTDB)
[HERE](https://gtdb.ecogenomic.org/downloads) and merge them with the NCBI results.
Here is an example with the bacterial metadata from GTDB `bac120_metadata_r220.tsv`.
You can also include the Life Identification NUmbers (LINs) from [genomeRxiv](http://genomerxiv.cs.vt.edu) or [LINflow](https://code.vt.edu/linbaseproject/LINflow) as well (optional). Similar to [Section 2.4](#24-tabulating-all-data) the order of the directories matters.

```bash
# python merge_gtdb.py ncbi_attributes.tsv gtdb_metadata.tsv linflow_summary.csv
python merge_gtdb.py attributes.tsv bac120_metadata_r220.tsv linflow_summary_scheme8.csv
```

When successful, your merged files can be found at `attributes_merged.tsv` and `attributes_merged.csv`.

### [2.7 Merging multiple GTDB attribute file versions](#27-merging-multiple-gtdb-attribute-file-versions)

If you would like to merge attributes from multiple versions of GTDB you can use the script below.

```bash
# python concat_gtdb_versions.py attribute207.tsv [attribute220.csv ...] output_file.[tc]sv
python concat_gtdb_versions.py bac120_metadata_r220.tsv bac120_metadata_r207.tsv
```

## [3 Advance Uses](#3-advance-uses)

## [3.1 Parallelizing the NCBI API requests](#31-parallelizing-the-ncbi-api-requests)

To parallelize the NCBI API requests, you need to sign up for an NCBI E-utilities API key best explained at [https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities/](https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities/).

When finished you can open the `get_all_data.sh` file and

1. Uncomment the line below
1. Paste it as the value (after the equal sign)
1. Change the parameter `REQUEST_BATCH=1` up to 4 (5 or above is usually unstable)

```bash
export NCBI_API_KEY="PASTE_YOUR_KEY_HERE"
```

**IMPORTANT: Please do not increase `REQUEST_BATCH` value (to greater than one) if you do not have an API key, otherwise your IP address will get blocked by NCBI permanently.**

### [3.2 Parallelizing the tabulation](#32-parallelizing-the-tabulation)

The tabulation process is already parallelized but you can increase the core utility
by changing the file `read_data.py` on the line `CORE_COUNT=` to increase core utilization.

### [3.3 Disabling directory field counts](#33-disabling-directory-field-counts)

If you have more than 100,000 queries the field count will take a couple of minutes to resolve.
If you wish to skip this step, only excludes the `*_counts.tsv` files, you can comment the for
loop in `read_data.py` that performs this task like so.

```python
    '''for dirpath in dirnames:
        combine_save_counts(list_dict_counts, dirpath)'''
```

### [3.4 Adding static fields to attributes](#34-adding-static-fields-to-attributes)

You can easily add fields (columns) to the `attributes` files if you are looking
for more fine-tuned information that is not included in the default columns (you can change the headers too).
For this, look at the dictionary variable `attribute_index` in the file `read_data.py`.
Each **key is the name of the column** and the value is a list of regular expression (regex) string(s) that
will be used to look for the fields in the downloaded files from NCBI.

For example:

```python
"region" : ["^region$","geographic location \\(.*region"],
```

This column has two regex strings matching regional data. This is ordered, where if the first regex matches, the second is ignored but if the second one matches is it only used if the first is not matched or empty. Our definition of missing is found in the variable `missing`. Keep in mind that the parenthesis is escaped `\\(` due to the string being used as a regex string (this will throw an error if a close parenthesis is not included for grouping).

### [3.5 Adding conditional fields to attributes](#35-adding-conditional-fields-to-attributes)

Conditional fields work similarly to static fields ([Section 3.4](#34-adding-static-fields-to-attributes))
with an extra option to run functions for data cleanup (e.g. create a uniform True/False value with multiple patterns).
For this, look at the dictionary variable `attribute_index_conditional` in the file `read_data.py`.
Now the key is the column name, the value is another dictionary with a "condition" and "patterns" keys.
The patterns value is a list of patterns to match the field header (similar to static fields).
The condition value is a function that processes the matched field's value.

For example:

```python
"environmental_sample": {
        "condition": lambda x: "True"
        if np.any(
            [
                re.match(r"" + pattern, x, re.IGNORECASE) is not None
                for pattern in ["^ye?s?", "true", "^1$"]
            ]
        )
        and not_null(x)
        else "False",
        "patterns": ["environmental.sample"],
    },
```

This creates a column called "environmental_sample" in the `attributes` files and if it finds a field header in the files matching the "environmental.sample" regex, it processes the value. If the value of that field matches any of the patterns `["^ye?s?", "true", "^1$"]`, it assigns a True otherwise a False. Conversely, if it does not find a field header matching "environmental.sample" it keeps the value as unknown (nan in python).

### [3.6 Removing fields from attributes](#36-removing-fields-from-attributes)

For this, look at the dictionary variable `attribute_index` and `attribute_index_conditional` in the file `read_data.py`.
Remove the key-value pair where the key is the column name you would like to exclude from your `attributes` file. Done!

### [3.7 get_all_data.sh parameters](#37-get_all_datash-parameters)

The file `get_all_data.sh` takes in multiple parameters.

```bash
bash get_all_data.sh \  # calling the script
email@example.edu \ # email you are using with or without your NCBI API key
biosample \         # database you want the API to search for your query
all_ids.dsv \       # file to look for IDs
3 \                 # column to as query use when the file is dollar separated (dsv)
data/biosample      # directory to dump files to. skips duplicate non-empty files
```

# parallel download genome files