#!/bin/bash
bash get_all_data.sh email@example.abc assembly all_ids.dsv 1 pantoea/assembly
python read_data.py ids pantoea/assembly 
bash get_all_data.sh email@example.abc biosample all_ids.dsv 3 pantoea/biosample
bash get_all_data.sh email@example.abc taxonomy all_ids.dsv 6 pantoea/taxonomy
python read_data.py all pantoea/assembly pantoea/biosample pantoea/taxonomy
cp *_counts.tsv pantoea/

python concat_gtdb_versions.py bac120_metadata_r220.tsv bac120_metadata_r207.tsv bac120_metadata_gtdb207-220.tsv
python concat_gtdb_versions.py gtdb220/attributes.csv gtdb207_rep/attributes3.csv gtdb207_nrep/attributes.csv gtdb207-220_attributes.csv

python merge_gtdb.py gtdb207_all/attributes3.tsv bac120_metadata_r207.tsv gtdb207_all/linflow_summary_gtdb_50k_v1.csv
