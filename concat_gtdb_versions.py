#!/usr/bin/env python3
"""  
# data from inputs to the left have priority
# e.g if gps is available in input1 and input2, input1 gets priority and the final table will be from input1
./concat_gtdb_versions.py <input1> <input2> ... <output>
./concat_gtdb_versions.py bac120_metadata_r220.tsv bac120_metadata_r207.tsv bac120_metadata_gtdb207-220.tsv
./concat_gtdb_versions.py gtdb220/attributes.csv gtdb207_rep/attributes3.csv gtdb207_nrep/attributes.csv gtdb207-220_attributes.csv
"""

import os.path
import sys

import numpy as np
import pandas as pd


def get_sep(path):
    sep = ","
    if path[-3:] == "tsv":
        sep = "\t"
    elif path[-3:] == "dsv":
        sep = "$"
    return sep, path[-3:]


def main(files, output, **kwargs):
    dfs = []
    for idx, file in enumerate(files):
        path = os.path.realpath(file)
        if not os.path.isfile(path):
            raise FileNotFoundError(path)
        sep, _ = get_sep(path)
        dfs += [
            pd.read_csv(file, header=0, skip_blank_lines=True, index_col=0, sep=sep)
        ]
        print(f"File '{file}' read into memory")

    df_concat = pd.concat(dfs).reset_index()
    print("Concatenation Done")

    index_name = np.setdiff1d(df_concat.columns, dfs[0].columns)[0]
    df_concat = df_concat.drop_duplicates(subset=index_name, keep="first")

    sep, _ = get_sep(output)
    df_concat.to_csv(output, sep=sep, index=False)
    print(f"Concatenation ({len(df_concat)} lines) saved to '{output}'")


if __name__ == "__main__":
    main(sys.argv[1:-1], sys.argv[-1])
