#!/bin/bash
#bash rename_files.sh assembly2/ all_ids3.txt

indir=$(realpath $1)
new_id_file=$2
for file in `ls $indir`
do
	filepath="$indir/$file"
	assembly_id=$(grep "AssemblyAccession" $filepath | awk -F"\"" '{print $4}')
	mv $filepath $indir/${assembly_id}.json
	echo $assembly_id >> $new_id_file
done
