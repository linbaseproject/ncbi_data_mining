#!/usr/bin/env python3
import copy
import csv
import json
import multiprocessing as mp
import os
import re
import sys
import time
import traceback
from glob import glob
from typing import Tuple

import alive_progress
import numpy as np
import pandas as pd

SAMPLE_SIZE = 100
CORE_COUNT = 10

missing = [
    "missing",
    "^n.?a\.?$",
    "not.applicable",
    "unknown",
    "not.collected",
    "not.determined",
    "none",
    "null",
    "^other$",
    "^-$",
    "withheld",
    "unclear",
    "not.provided",
    "not.recorded",
    "^\?$",
    "not.available",
    "cannot.be.opened",
]
strain_abbreviations = [".*type.strain:?\s?", ".*str\.\s?", ".*strain:?\s?"]
float_regex = "(\d+\.?\d*)"

# this is a list of keys and their corresponding keys within the NCBI data
# the first key will be the output column name
# the second will be a set of regex patterns in order of priority whose value
# will be extracted from the NCBI data
# (see attribute_index_conditional for a filtering condition)
attribute_index = {
    # Taxonomy metadata
    "superkingdom": ["^superkingdom$"],
    "phylum": ["^phylum$"],
    "class": ["^class$"],
    "order": ["^order$"],
    "family": ["^family$"],
    "genus": ["^genus$"],
    "species": ["^species$"],
    "subspecies": ["^subspecies$"],
    "pathovar": ["^pathovar$"],
    "race": ["^race$"],
    "biovar": ["^biovar$"],
    "clade": ["^clade$"],
    "genomospecies": ["^genomospecies$"],
    "phylogroup": ["^phylogroup$"],
    "phylotype": ["^phylotype$"],
    "serotype": ["^serotype$"],
    "serovar": ["^serovar$"],
    "biotype": ["^biotype$"],
    "sequevar": ["^sequevar$"],
    # biosample metadata (taxonomy metadata can also be found and is checked for)
    "geo_loc_name": [
        "geo_loc_name",
        "geo.*loc.*name",
        "geographic location \\(country",
        "geographic",
        "country",
        "source.area",
    ],
    "region": ["^region$", "geographic location \\(.*region"],
    "ocean_region": ["longhurst.province"],
    "Isolation_GPS_Coordinates": ["lat.*lon", "latitude.(and.)?longitude"],
    "latitude": ["latitude"],
    "longitude": ["longitude"],
    "assembly_program": ["^assembly.method$", "assembly.name", "assembly.software"],
    "assembly_version": ["assembly_method_version"],
    "collected_by": ["collected.by"],
    "email": ["^email$"],
    "first": ["^First$"],
    "middle": ["^Middle$"],
    "last": ["^Last$"],
    "isolation_source": [
        "isolation.*source",
        "isolate.source",
        "env.biome",
        "feature",
        "biome",
        "^material$",
        "isolation.site",
        "environmental.package",
        "habitat",
        "env.material",
        "wastewater.type" "^host.sample$",
        "^environment",
        "^sample.type$",
        "source.of.isolate",
        "tissue.type",
    ],
    "phenotype": ["phenotypes"],
    "disease": ["host.disease", "^diseases$"],
    "sequencing_technology": ["seq(uencing)?.meth(od)?", "seq(uencing)*.tech(nolgy)*"],
    # Assembly metadata
    "bioproject": ["^BioprojectAccn$"],
    "biosample": ["^BioSampleAccn$"],
    "contigs": ["contig_count"],
    "total_length": ["total_length"],
    "ncbi_accession": ["^AssemblyName$"],
    "ncbi_assembly_acc": ["^AssemblyAccession$"],
    "ncbi_taxid": ["^Taxid$"],
    "ncbi_genus-species": ["^Organism$"],
    "biosample_package": ["^Package$"],
    "ncbi_type-material": [
        "type.material",
        "type.strain",
        "status",
        "$type$",
        "type.status",
    ],
    # "ncbi_type-relation": ["^FromType$"],
}

# similar to attribute_index with a filtering condition
attribute_index_conditional = {
    "ncbi_type_strain": {
        "condition": lambda x: (
            "True"
            if np.any(
                [
                    re.match(r"" + pattern, x, re.IGNORECASE) is not None
                    for pattern in ["type.strain"]
                ]
            )
            # and not_null(x)
            else "False"
        ),
        "patterns": [
            "type strain",
        ],
    },
    "ncbi_representatives": {
        "condition": lambda x: (
            "True"
            if np.any(
                [
                    re.match(r"" + pattern, x, re.IGNORECASE) is not None
                    for pattern in [
                        "type.strain",
                        "^ye?s?",
                        "true",
                        "^1$",
                        "type.material",
                        "neotype",
                    ]
                ]
            )
            # and not_null(x)
            else "False"
        ),
        "patterns": [
            "type.material",
            "type.strain",
            "^FromType$",
            "status",
            "type",
            "type.status",
        ],
    },
    "environmental_sample": {
        "condition": lambda x: (
            "True"
            if np.any(
                [
                    re.match(r"" + pattern, x, re.IGNORECASE) is not None
                    for pattern in ["^ye?s?", "true", "^1$"]
                ]
            )
            and not_null(x)
            else "False"
        ),
        "patterns": ["environmental.sample"],
    },
    "plant_sample": {
        "condition": lambda x: x.strip() if not_null(x) else "",
        "patterns": ["plant_body_site"],
    },
    "isolation_host": {
        "condition": lambda x: str.lower(x),
        "patterns": [
            "^host$",
            "specific.host",
            "nat.host",
            "host.scientific.name",
            "host.(common)?.?name",
            "host.infra.specific(.name)?",
            "host.substrate",
            "host.species",
            "isolate.host",
        ],
    },
    "isolation_date": {
        "condition": lambda x: clean_date(str(x)) if x is not np.nan else None,
        "patterns": ["collection.*date"],
    },
    "coverage": {
        "condition": lambda x: parse_coverage(x),
        "patterns": ["genome.coverage"],
    },
    "clade": {
        "condition": lambda x: (
            re.sub("clade\s?", "", x).strip() if x is not None else None
        ),
        "patterns": ["^clade$"],
    },
    "ncbi_download_path": {
        "condition": lambda x: (
            re.sub("assembly_report.txt", "genomic.fna.gz", x).strip()
            if x is not None
            else None
        ),
        "patterns": ["^FtpPath_Assembly_rpt$"],
    },
    "ncbi_filename": {
        "condition": lambda x: (
            os.path.basename(re.sub("assembly_report.txt", "genomic.fna.gz", x).strip())
            if x is not None
            else None
        ),
        "patterns": ["^FtpPath_Assembly_rpt$"],
    },
}


# checks if the string is not None or a missing/unknown value
def not_null(text: str) -> bool:
    if text is None:
        return False
    return not np.any(
        [re.match(r"" + el, text.strip(), re.IGNORECASE) for el in missing]
    )


# checks if all strings is the list are keys in the dictionary
# exact parameters allows pattern matching (non exact matching)
def keys_exist(query: list, obj: dict, exact=True):
    if type(obj) is not dict:
        return False
    if exact:
        return np.all([el in obj.keys() for el in query])
    else:
        return np.all(
            [
                np.any(
                    [
                        (
                            True
                            if re.match(r"" + el, key, re.IGNORECASE) is not None
                            else False
                        )
                        for key in obj.keys()
                    ]
                )
                for el in query
            ]
        )


def get_seq_ids(entry):
    ids_dict = {}
    ids = get_dict_leafs(entry["Seq-entry_seq"]["Bioseq"]["Bioseq_id"]["Seq-id"])
    for id in ids:
        id_key = list(id.keys())[0]
        if id_key not in ids_dict:
            ids_dict.update(id)
        else:
            print("duplicate key:", id_key, ids_dict[id_key])
    return ids_dict


# traverses json structure (dict and list) to get the leafs + some irregulars as if statements
def get_dict_leafs(obj):
    all = []
    # None empty string, list, dict
    if obj is None or len(obj) < 1:
        return []
    elif keys_exist(["content", "category"], obj):
        return [{obj["category"]: obj["content"]}]
    elif keys_exist(["content", "attribute_name"], obj):
        # biosample multi-key dict
        return [{obj["attribute_name"]: obj["content"]}]
    elif keys_exist(["Sub_type", "Sub_value"], obj):
        # sub-section dictionary
        return [{obj["Sub_type"]: obj["Sub_value"]}]
    elif keys_exist(["db", "content"], obj, exact=False):
        # database dictionary
        db_keys = [
            key for key, value in obj.items() if re.match(r"db", key) is not None
        ]
        if db_keys is not None:
            return [{obj[db_keys[0]]: obj["content"]}]
    elif keys_exist(["Rank", "TaxId", "ScientificName"], obj) and len(obj.keys()) <= 4:
        # taxonomy specific rank dict
        return [{obj["Rank"]: obj["ScientificName"]}]
    # dict_index list_index, dict_key list_value
    for di_li, dk_lv in enumerate(obj):
        key_index = di_li if type(obj) is list else dk_lv
        # skip keys
        if key_index in ["Seqdesc_pub", "Seq_comment", "Seqdesc_user"]:
            continue

        # when the obj is a dict and value is primitive
        if type(obj) is dict and type(obj[key_index]) in [str, int, float, bool]:
            all += [{key_index: obj[key_index]}]
        # remove unwanted intermediary keys
        elif type(obj[key_index]) is dict and keys_exist(["string"], obj[key_index]):
            all += [({key_index: obj[key_index]["string"]})]
        # when the obj is a list we split the primitive and non-primitive values in the list
        elif type(obj[key_index]) is list:
            primitive_elements = list(
                filter(lambda x: type(x) in [str, int, float, bool], obj[key_index])
            )
            if len(primitive_elements) > 0:
                all += [{key_index: primitive_elements}]
            all += copy.deepcopy(
                get_dict_leafs(
                    list(
                        filter(
                            lambda x: type(x) not in [str, int, float, bool],
                            obj[key_index],
                        )
                    )
                )
            )
        # other cases like base cases
        else:
            all += copy.deepcopy(get_dict_leafs(obj[key_index]))
    return all  # this is always a list


# gets all key,value pairs whose keys matches the pattern
def get_key(leaf_list: list, pattern: str) -> list:
    result = []
    # pattern = re.escape(pattern)
    regex = r"" + pattern
    for el in leaf_list:
        for index, key in enumerate(el):
            for i in range(2):
                try:
                    if re.search(regex, key, re.IGNORECASE):
                        result.append({key: el[key]})
                    break
                except Exception:
                    print(f"error on regex '{regex}' in key '{key}' attempt {i}")
                    regex = r"" + re.escape(pattern)
                    if i > 1:
                        traceback.print_exc()
    return result


# gets first key,value pair whose key matches the pattern
def get_first_value(leaf_list: list, pattern: str):
    all_matching_keys = get_key(leaf_list, pattern)
    try:
        return str(list(all_matching_keys[0].values())[0])
    except IndexError:
        return None


# takes the list of dicts and returns the value if key matches in the order of select list (prioritizes)
# it can also be manipulated by the condition parameter (disabled by default)
def ordered_key_select(leaf_list: list, select_list: list, condition=lambda x: x):
    for target in select_list:
        current_selection = get_first_value(leaf_list, target)
        if current_selection is not None and current_selection.strip() != "":
            return condition(current_selection.strip())
    return None


# parse NCBI json file to leafs safely
def parse_ncbi_json(filepath: str) -> list:
    if os.stat(filepath).st_size == 0:
        # if file empty
        return []
    try:
        with open(filepath) as json_file:
            d = json.load(json_file)

        # root = d['DocumentSummarySet']['DocumentSummary']
        return get_dict_leafs(d)
    except (json.JSONDecodeError, KeyError) as e:
        print("File Error {}\n".format(filepath))
        traceback.print_exc()
    return []


# returns a list of all keys of the leafs (mostly for counting)
def get_all_keys(leafs: list):
    # unique leaf keys for one file
    return np.unique(
        np.array(
            [list(el.keys()) if type(el) is dict else None for el in leafs]
        ).flatten()
    )


# get list of keys with their counts and 5 samples per key as a dict
# {key1:{count:123, samples:[s1,s2,...]}, key2: ...}
def get_key_counts(current_counts: dict, leafs: list) -> dict:
    to_add_keys = get_all_keys(leafs)
    available_keys = list(current_counts)

    new_keys = np.setdiff1d(to_add_keys, available_keys)
    old_keys = np.intersect1d(to_add_keys, available_keys)

    # add new keys to the collection
    for key in new_keys:
        current_counts[key] = {}
        current_counts[key]["count"] = 1
        # removes *,+,? from key to match words only and avoid wired regex match
        keyword = re.sub(r"[\*\?\+\(\)]+", ".", key)
        current_counts[key]["sample"] = [get_first_value(leafs, keyword)]

    # count old keys and append as sample if not counted
    for key in old_keys:
        current_counts[key]["count"] += 1
        samples = current_counts[key]["sample"]
        value = get_first_value(leafs, key)
        if len(samples) < SAMPLE_SIZE and value not in samples:
            current_counts[key]["sample"] += [value]
    return current_counts


# removes all patterns in list from string
def remove_all(string: str, pattern_list: list):
    for pattern in pattern_list:
        if string is None or string.strip() == "":
            return None
        string = re.sub(r"" + pattern, "", string)
    return string


# gets strain name from the elements in the order:
# strain, isolate, organism - species_name, or end of organism if long enough
def get_strain(leafs: list) -> Tuple[str, str]:
    strain = None
    category = None
    try:
        for leaf_name in ["strain", "isolate"]:
            strain = (
                get_first_value(leafs, leaf_name) if strain is None else strain.strip()
            )

        organism = get_first_value(leafs, "Organism")
        species_name = get_first_value(leafs, "SpeciesName")
        # category denoted in parentheses at the end or organism
        category_start = organism.rfind("(")
        category = organism[category_start + 1 : -1] if category_start > 0 else None
        if strain is not None:
            return strain, category

        # when there is an organism and species and more (hence >2)
        if species_name is not None and len(species_name.split(" ")) > 2:
            strain = species_name.split(" ")[-1]
        # when two parentheses enclosed strings exist
        elif species_name is not None and len(re.findall(r"(\([^)]+\))", organism)) > 1:
            relevant_chunk = organism[len(species_name) :]
            end_of_first_parentheses = relevant_chunk.find(")")
            start_of_second_parentheses = relevant_chunk.rfind("(")
            if end_of_first_parentheses + 3 <= start_of_second_parentheses:
                strain = relevant_chunk[
                    end_of_first_parentheses + 1 : start_of_second_parentheses
                ]
        # when Organism and SpeciesName does not match (get anything before parentheses)
        elif species_name is not None and organism[len(species_name) :].find("(") > 2:
            relevant_chunk = organism[len(species_name) :].strip()
            start_of_category = (
                (-1 * len(category) - 2)
                if category is not None
                else relevant_chunk.rfind("(")
            )
            strain = relevant_chunk[:start_of_category]
    except:
        pass
        # print("ERROR:\t{}\n".format(leafs))
        # print(traceback.print_exc())
    finally:
        return strain, category


# parses coverage values with different patterns
def parse_coverage(coverage: str):
    if not not_null(coverage):
        return None
    numbers = re.findall(r"" + float_regex, coverage)
    # skip the level number within the coverage string if it exists
    pick = 1 if re.match(r"level", coverage, re.IGNORECASE) else 0
    if len(numbers) > pick:
        return numbers[pick]


# parses assembly software e.g. (name v1.2.3; name2 4.5.6 ...) (depricated)
def parse_assembly(assembly1: str):
    if not not_null(assembly1):
        return
    assembly = re.sub(r";\s*;", ";", assembly1)
    try:
        matches = [
            re.findall(r"([^.]+)[\sv.]*(\w?[\d.\-_/]*)", el, re.IGNORECASE)
            for el in assembly.split(";")
        ]
        assemblies = [
            remove_all(el[0][0], ["\s."]) if el[0][0] is not None else None
            for el in matches
        ]
        versions = [el[0][1] if el[0][1] is not None else None for el in matches]
        return "; ".join(assemblies), "; ".join(versions)
    except IndexError:
        return assembly, None


def clean_date(date):
    if date is None or pd.isna(date) or len(date) < 1:
        return None
    matches = len(re.findall(r"/", date))
    try:
        return pd.to_datetime(date).strftime("%m/%d/%Y")
    except Exception:
        pass
    if matches == 1:
        date = date[: date.find("/")]
    elif 1 < matches < 4:
        date = date.strip()
    else:
        years = re.findall(r"\d{4}", date)
        if len(years) > 0:
            date = years[0]
    try:
        return pd.to_datetime(date).strftime("%m/%d/%Y")
    except Exception:
        return None


# Extracts attributes given the dictionary of key, patterns, (condition/filter) from the parsed leafs
def extract_attributes_leafs(
    leafs: list, attribute_index, conditional_attribute_index=None
) -> dict:
    attributes = {}
    if conditional_attribute_index is None:
        conditional_attribute_index = dict()

    for key, value in attribute_index.items():
        key_match = ordered_key_select(leafs, value)
        if key_match is not None and not_null(key_match):
            attributes[key] = key_match

    for key, value in conditional_attribute_index.items():
        key_match = ordered_key_select(leafs, value["patterns"], value["condition"])
        if key_match is not None and not_null(key_match):
            attributes[key] = key_match

    return attributes


# Extracts attributes given the dictionary of key, patterns, (condition/filter) from the file
def extract_attributes_file(
    filepath: str, attribute_index, conditional_attribute_index=None
) -> Tuple[list, dict]:
    if conditional_attribute_index is None:
        conditional_attribute_index = {}

    if not os.path.isfile(filepath):
        return [], {}
    leafs = parse_ncbi_json(filepath)
    if not leafs:
        return [], {}

    return leafs, extract_attributes_leafs(
        leafs, attribute_index, conditional_attribute_index
    )


# extracts all ids based on the patterns and saves the results
def get_ids(dirpath, outfile):
    attributes = {}
    attribute_index = {
        "bioproject": ["^BioprojectAccn$"],
        "biosample": ["^BioSampleAccn$"],
        "accession": ["^AssemblyName$"],
        "assembly": ["^AssemblyAccession$"],
        "taxid": ["^Taxid$"],
    }
    for file in glob(
        dirpath + "/*.json",
    ):
        leafs, record_attribute = extract_attributes_file(
            str(os.path.abspath(file)), attribute_index
        )
        attributes[os.path.basename(file)[:-5]] = record_attribute

    attribute_df = pd.DataFrame.from_dict(attributes, orient="index")
    attribute_df.to_csv(outfile, sep="$", header=False)


def get_attributes(filepaths: list, dirnames: list[str]) -> Tuple[dict, dict]:
    """
    retrieves the attributes for an accession given the matching files from
    different directories (e.g. assembly,biosample,taxonomy)
    returns ID's attributes and updates the possible attribute key counts (used and unused)
    """
    # initializing default values
    attributes = {}  # {"interest": "Undefined interest"}
    counts = {}
    for index, meta_filepath in enumerate(filepaths):
        dirname = os.path.basename(dirnames[index])
        leafs, file_attributes = extract_attributes_file(
            meta_filepath, attribute_index, attribute_index_conditional
        )
        attributes.update(file_attributes)
        # looking for strain name
        strain, category = get_strain(leafs)
        z = (
            attributes.update({"strain": remove_all(strain, strain_abbreviations)})
            if not_null(strain)
            else -1
        )
        attributes.update({"category": category})

        ## Handling irregular attributes from biosample

        tmp = (
            re.findall(r".*type.strain:?\s?(.*)$", strain) if strain is not None else []
        )
        if "Type strain" in attributes.keys() and len(tmp) > 0:
            attributes["Type strain"] = tmp[0]

        # this was intended to separate software from version (but had bugs)
        """if "Assembly program" in attributes.keys():
            assemblies, versions = parse_assembly(attributes["Assembly program"])
            if assemblies:
                attributes["Assembly program"] = assemblies
            if versions:
                attributes["assembly_version"] = versions"""
        # counts[f"{index}"] = get_key_counts(counts[index], leafs)
        counts[f"{dirname}"] = get_key_counts({}, leafs)

    return attributes, counts


def return_restart_time(stime):
    runtime_sec = time.time() - stime
    elapsed = time.strftime("%Hh%Mm%Ss", time.gmtime(runtime_sec))
    return elapsed, time.time()


def get_accession_attributes(
    stime: float, dirnames: list[str], count: int, file_count: int, assembly_file: str
):
    """reads all files related to an accession and saves their attributes in all_attributes and returns the key counts

    Args:
        stime (float): starting time in sec
        dirnames (list[str]): path of dirs to find matching files (by assemblyID)
        count (int): count of the current file
        file_count (int): total file count
        assembly_file (str): current assembly file for ID reference

    Returns:
        filename: "GCA_123456789.json"
        counts: dict of key counts {"assembly":{"key":{"count":int,"sample":list[values:str]}}, "biosample":{}}
        attributes: dict of matched attributes {"attribvute": value}
    """

    # global all_attributes
    filename = os.path.basename(assembly_file)
    file_list = [
        os.path.abspath(os.path.join(dirname, filename)) for dirname in dirnames
    ]
    attributes, counts = get_attributes(file_list, dirnames)
    # all_attributes[os.path.basename(assembly_file)[:-5]] = attributes

    """if count % 1000 == 0:
        elapsed, stime = return_restart_time(stime)
        print(f"{count}/{file_count}\t{count*100/file_count:.2f}%\t{elapsed}")"""
    return (filename, counts, attributes)


def sum_flatten_limit_list(obj_list: list[list], limit=SAMPLE_SIZE):
    """Flattens a list of list and only returns a list of size limit

    Args:
        obj_list (list[list]): to flatten
        limit (int, optional): returned list length limit. Defaults to SAMPLE_SIZE.

    Returns:
        list: flattened list
    """
    flat_dict = set()
    for row in obj_list:
        if isinstance(row, list):
            for row_sub in row:
                flat_dict.add(row_sub)
        else:
            flat_dict.add(row)
        if len(flat_dict) > limit:
            return list(flat_dict)
    return list(flat_dict)


def combine_save_counts(list_dict_counts: list[dict], dirpath: str):
    """Combines the counts from ever file count and aggregates and saves them into a tsv file

    Args:
        list_dict_counts (list[dict]): One dictionary of counts per file within each where the keys are the leaf attributes available
        dirpath (str): counts specifically for one directory
    """
    dirname = os.path.basename(dirpath)
    count_sample_df = pd.concat(
        [
            pd.DataFrame.from_dict(accession_count_dict[dirname], orient="index")
            for accession_count_dict in list_dict_counts
            if accession_count_dict[dirname]
        ]
    )
    count_sample_df.reset_index().groupby("index").agg(
        {"count": "sum", "sample": sum_flatten_limit_list}
    ).to_csv(os.path.basename(dirname) + "_counts.tsv", sep="\t")
    print(f"There were {len(count_sample_df):,} many leaves in {dirname}")


def gps_to_decimal(lat_lon_gps: str, return_str=False):
    """Converts combined latlong to decimal GPS e.g. '59.91 N 10.73 W' => 59.91, -10.73"

    Args:
        lat_lon_gps (str): string gps containing latitude and longitude with degree and compass heading

    Returns:
        list[float,float]: latitude, longitude
    """
    if not lat_lon_gps:
        if return_str:
            return ""
        return []
    lat = lon = 0
    if m := re.match(
        r"(-?\d+\.(\d+))[^\d]*([NnsS])\s(-?\d+\.\d+)[^\d]*([EewW])", lat_lon_gps.strip()
    ):
        # g1 lat decimal, g2 lat compass, g3 lon decimal, g4 lon compass
        decimal_points = len(m.group(2))
        try:
            lat = float(m.group(1))
            lon = float(m.group(4))
            if m.group(3).lower() == "s" and lat > 0:
                lat *= -1
            if m.group(5).lower() == "w" and lon > 0:
                lon *= -1

        except ValueError as e:
            traceback.print_exc()
            if return_str:
                return ""
            return []
        if return_str:
            return f"{lat:.{decimal_points}f} {lon:.{decimal_points}f}"
        return [lat, lon]


if __name__ == "__main__":
    # skeleton = "esearch -email email@example.edu -db {0} -query {1} | efetch -format docsum -json >{2}/{3}.json"
    all_attributes = {}

    stime = time.time()
    mode = sys.argv[1]
    dirnames = [os.path.abspath(path) for path in sys.argv[2:]]
    file_counts = [len(glob(dirpath + "/*.json")) for dirpath in dirnames]

    # if there are any files missing (skips empty)
    if not np.all([count == file_counts[0] for count in file_counts]):
        print("Your file counts do not match between the directories", file_counts)
    files = glob(dirnames[0] + "/*.json")
    count = 0
    key_counts = [{} for i in range(len(dirnames))]

    if mode == "ids":
        get_ids(dirnames[0], "all_ids.dsv")
        print("Successful. Saved in all_ids.dsv")
        exit(0)

    # python read_data.py weird_id.csv 1 all_ids.tsv xxx/assembly
    if mode == "rename":
        src = sys.argv[2]
        src_col = sys.argv[3]
        dest = sys.argv[4]
        rename_dir = dirnames[5]

        if src[-3:] == "tsv":
            sep = "\t"
        elif src[-3:] == "dsv":
            sep = "$"
        else:
            sep = ","
        try:
            src_data = pd.read_csv(src, header=0, sep=sep)[int(src_col - 1)]
        except IndexError:
            print(f"Column {src_col} does not exist in {src}")
        except ValueError:
            print(f"The entered value {src_col} is not a valid column number (>0)")

        dest_data = pd.read_csv(dest, header=None, sep="\t")[0]
        for idx, filename in enumerate(src_data):
            filepath = os.path.join(rename_dir, f"{filename}.json")
            if os.path.isfile(filepath):
                os.rename(filepath, os.path.join(rename_dir, f"{idx}"))
            else:
                print(f"File did not exist {filepath}")

    tic = time.perf_counter()
    POOL = mp.Pool(CORE_COUNT)
    elapsed, stime = return_restart_time(stime)
    print(f"init took {elapsed}")

    file_count = len(files)
    process_outputs = []
    list_dict_counts: list[dict[str, int]] = []
    for file in files:
        process_outputs += [
            POOL.apply_async(
                get_accession_attributes, (stime, dirnames, count, file_count, file)
            )
        ]
        count += 1

    with alive_progress.alive_bar(
        file_count,
        title="Parsing attributes from files",
    ) as progress:
        for process in process_outputs:
            # if process is a thread output that has to be waited for wait to get the result
            if not isinstance(process, tuple):
                process = process.get()
            filename = process[0]
            list_dict_counts += [process[1]]
            all_attributes[filename[:-5]] = process[2]
            progress()
    print("Stopped and consolidated threads")
    elapsed, stime = return_restart_time(stime)
    print(f"attributes took {elapsed}")

    attribute_df = pd.DataFrame.from_dict(all_attributes, orient="index")

    if "geo_loc_name" in attribute_df.keys():
        country_df = attribute_df["geo_loc_name"].str.split(":", n=1, expand=True)
        attribute_df["Country"] = country_df[0].str.strip()
        if 1 not in country_df.columns:
            country_df[1] = np.nan
            country_df[1] = country_df[1].astype(attribute_df["Country"].dtype)
        region = country_df[1].str.strip()

        attribute_df.drop(columns=["geo_loc_name"], inplace=True)
        if "region" in attribute_df.keys():
            attribute_df["Region"] = region.combine_first(attribute_df["region"])
            attribute_df.drop(columns=["region"], inplace=True)
        else:
            attribute_df["Region"] = region

    if "Isolation_GPS_Coordinates" in attribute_df.keys():
        # "Converting '59.91 N 10.73 W' => 59.91, -10.73"
        attribute_df["Isolation_GPS_Coordinates"] = (
            attribute_df["Isolation_GPS_Coordinates"]
            .astype(str)
            .apply(lambda x: gps_to_decimal(x, return_str=True))
        )

    # combine separate lat lon columns into the main column
    if np.all(
        [
            col in attribute_df.keys()
            for col in ["Isolation_GPS_Coordinates", "latitude", "longitude"]
        ]
    ):
        combine_lat_lon = attribute_df[["latitude", "longitude"]].apply(
            lambda x: " ".join(x.dropna().astype(str).values), axis=1
        )  # .astype(attribute_df["Isolation_GPS_Coordinates"].dtype)
        combine_lat_lon = combine_lat_lon.replace("", np.nan)
        attribute_df["Isolation_GPS_Coordinates"] = attribute_df[
            "Isolation_GPS_Coordinates"
        ].combine_first(combine_lat_lon)
        attribute_df.drop(columns=["latitude", "longitude"], inplace=True)

    elapsed, stime = return_restart_time(stime)
    print(f"geo-location took {elapsed}")

    if "Date of isolation" in attribute_df.keys():
        date_df = pd.to_datetime(attribute_df["Date of isolation"], errors="coerce")
        attribute_df["Date of isolation"] = date_df.dt.strftime("%m/%d/%Y").replace(
            "NaT", ""
        )
    elapsed, stime = return_restart_time(stime)
    print(f"dates took {elapsed}")

    attribute_df.to_csv("attributes.csv", sep=",", quoting=csv.QUOTE_ALL)
    attribute_df.to_csv("attributes.tsv", sep="\t")
    print(f"Attributes Saved to '{os.path.abspath('attributes.tsv')}'")

    del attribute_df
    elapsed, stime = return_restart_time(stime)
    print(f"Saving took {elapsed}")

    for dirpath in dirnames:
        combine_save_counts(list_dict_counts, dirpath)

    elapsed, stime = return_restart_time(stime)
    print(f"count files took {elapsed}")

    print("Complete!")
