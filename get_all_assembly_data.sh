#!/bin/bash
#Usage:
#bash get_all_assembly_data.sh email@zz.com assembly_id_file.txt assembly/

#export PATH=$PATH:/home/rmazloom/idea_projects/linbase/gtdb/
email="$1"
id_file=$(realpath $2)
total=$(wc -l $id_file | cut -f 1 -d " ")
count=0
outdir=$(realpath $3)
mkdir -p $outdir
for id in $(cat $id_file)
do
	if ! [ -s $outdir/${id}.json ]
	then
		esearch -email $email -db assembly -query $id | efetch -format docsum -json >$outdir/${id}.json;
		sleep 3;
	fi
	count=$(( $count + 1 ));
	echo "$count / $total";
done
