#!/bin/bash
# Usage:
# bash get_all_data.sh email@zzz.com biosample all_ids.dsv 3 biosample/

# REQUEST_BATCH=1 without key OR REQUEST_BATCH=4 with key
# export NCBI_API_KEY="PASTE_YOUR_KEY_HERE"

REQUEST_BATCH=1
email="$1"
category="$2"
id_file=$(realpath "$3")
column=$4
outdir=$(realpath "$5")
total=$(wc -l $id_file | cut -f 1 -d " ")
count=1
mkdir -p $outdir
while IFS="" read -r line || [ -n "$line" ]
do
	assembly=$(echo $line | cut -f 1 -d "$")
	id0=$(echo $line | cut -f $column -d "$") #biosampleID
	id=${id0//[$'\t\r\n']}

	# if the file does not exist
	if ! [ -s "${outdir}/${assembly}.json" ]
	then
		# if the search database is taxonomy
		if [ $category = "taxonomy" ] #&& ! [ -s "${outdir}/${assembly}.json" ]
		then
			# if the taxid file is not already available
			if ! [ -s "${outdir}_temp/${id}.json" ]
			then 
				mkdir -p "${outdir}_temp/"
				( efetch -db taxonomy -id "$id" -format xml -json > "${outdir}_temp/${id}.json" && cp "${outdir}_temp/${id}.json" "${outdir}/${assembly}.json") &
			else
				cp "${outdir}_temp/${id}.json" "${outdir}/${assembly}.json"
				count=$(( $count + 1 ))
				# skips the sleep if the taxonomy file already exists
				continue
			fi
		else	
			esearch -email $email -db "$category" -query "$id" </dev/null| efetch -format docsum -json > "${outdir}/${assembly}.json" &
		fi
		# this creastes a 2sec wait between batch runs ar % <num>
		if (( $count % REQUEST_BATCH == 0))
		then
			sleep 2
		fi
		echo "$count / $total : Looking for $id"
	else
		# when doing reruns, this shows up when runs are skipped (since the files were available and non-empty in output)
		if (( $count % 10000 == 0))
		then
			echo "$count / $total : Passed"
		fi
	fi
	
	count=$(( $count + 1 ))
done < $id_file
echo "Complete!"
