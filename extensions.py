import os.path
import re
from typing import Tuple

ACCEPTED_EXTENSIONS = {
    "tar": {
        "extensions": [".tar.bz2", ".tar.xz", ".tar", ".tar.gz", ".tgz"],
        # "function":     co.convert2tgz,
    },
    "raw": {
        "extensions": [".fasta", ".fna", ".fa", ".tmp"],
        # "function":     co.compress,
    },
    "gzip": {
        "extensions": [".gz"],
    },
    "sourmash": {"extensions": [".sig"]},
}


def find_extension(source: str) -> Tuple[str, str]:
    """Find file type (see ACCEPTED_EXTENSIONS)
    Args:
            source (str): input genome file path

    Returns:
            str: extension group
            str: extension string
    """
    filename = os.path.basename(source)
    for key, value in ACCEPTED_EXTENSIONS.items():
        for extension in value["extensions"]:
            pattern = re.compile(rf".+{extension}$", flags=re.IGNORECASE)
            match = pattern.fullmatch(filename)
            if match:
                return key, extension
    raise Exception(f"File Extension not supported {source}")


def remove_extension(
    path: str, recursive=True, original=None, raise_exception=True
) -> str:
    """removes the extension of a file

    Args:
            path (str): path to file
            recursive (bool): if extension should be removed recursively
            original (str): used internally should be left None

    Returns:
            str: removes the recognized extensions of the file (recursively if requested)
    """
    no_extension = True
    try:
        _, extension = find_extension(path)
        no_extension = os.path.basename(path[: -1 * len(extension)])
        if recursive:
            no_extension = remove_extension(no_extension, original=path)
    except Exception as exception:
        # raised if the file has no extensions
        if original is None:
            if raise_exception:
                raise exception
        return path
    return no_extension if no_extension else path
